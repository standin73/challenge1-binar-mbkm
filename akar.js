const readline = require('readline');

const akar = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const hasil = () => {
    akar.question('Masukkan angka ? ', (data) => {
    const value = parseFloat(data);
    const hasil = Math.sqrt(value)
    console.log(`Hasil dari akar ${data} adalah ${hasil}` );
    akar.close();
    });
}

hasil();