const readline = require('readline');

const kuadrat = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const hasil = () => {
    kuadrat.question('Masukkan angka ? ', (data) => {
    const value = parseFloat(data);
    const hasil = parseFloat(value * value);
    console.log(`Hasil dari kuadrat ${data} adalah ${hasil}` );
    kuadrat.close();
    });
}

hasil();