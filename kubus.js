
const readline = require('readline');

const volume = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});



const kubus = () => {
    volume.question('Masukkan sisi dari kubus? ', (sisi) => {
    const value = parseFloat(sisi);
    const hasil = parseFloat(value * value * value);
    console.log(`volume kubus dari ${sisi} adalah ${hasil}` );
    volume.close();
    });
}

kubus();