let inputArr = [70,82,76,89,90];
let bubbleSort = (inputArr) => {
    let len = inputArr.length;
    for (let i = 0; i < len; i++) {
        for (let j = 0; j < len; j++) {
            if (inputArr[j] > inputArr[j + 1]) {
                let tmp = inputArr[j];
                inputArr[j] = inputArr[j + 1];
                inputArr[j + 1] = tmp;
            }
        }
    }
    return inputArr;
}

let nilaiTertinggi = bubbleSort(inputArr).reverse()[0];
let nilaiTerendah = bubbleSort(inputArr)[0];
console.log(`Nilai tertinggi dari array terserbut adalah ` + nilaiTertinggi)
console.log('Nilai terendah dari array tersebut adalah ' + nilaiTerendah)